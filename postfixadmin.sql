-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: postfixadmin
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `superadmin` tinyint(1) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `modified` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `phone` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `email_other` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `token` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `token_validity` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Postfix Admin - Virtual Admins';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('superadmin@mx108.in.ddcpl.com','$1$11c5c0ec$5RGRKgUC6rDyU6.4WDlkn0',1,'2020-10-24 06:34:48','2020-10-24 06:34:48',1,'','','','2020-10-24 06:34:48');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alias`
--

DROP TABLE IF EXISTS `alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alias` (
  `address` varchar(255) NOT NULL,
  `goto` text NOT NULL,
  `domain` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `modified` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`address`),
  KEY `domain` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Postfix Admin - Virtual Aliases';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alias`
--

LOCK TABLES `alias` WRITE;
/*!40000 ALTER TABLE `alias` DISABLE KEYS */;
INSERT INTO `alias` VALUES ('abuse@harshill.com','abuse@mx108.in.ddcpl.com','harshill.com','2020-11-12 20:04:35','2020-11-12 20:04:35',1),('abuse@justserverz.com','abuse@mx108.in.ddcpl.com','justserverz.com','2020-10-25 05:48:54','2020-10-25 05:48:54',1),('abuse@kcplca.com','abuse@mx108.in.ddcpl.com','kcplca.com','2020-12-14 19:17:57','2020-12-14 19:17:57',1),('abuse@mx108.in.ddcpl.com','abuse@mx108.in.ddcpl.com','mx108.in.ddcpl.com','2020-10-24 06:35:15','2020-10-24 06:35:15',1),('admin@harshill.com','admin@harshill.com','harshill.com','2020-11-12 21:25:16','2020-11-12 21:25:16',1),('anto@justserverz.com','anto@justserverz.com','justserverz.com','2020-10-25 05:49:44','2020-10-25 05:49:44',1),('anto@mx108.in.ddcpl.com','anto@mx108.in.ddcpl.com','mx108.in.ddcpl.com','2020-10-24 07:08:25','2020-10-24 07:08:25',1),('demo2@mx108.in.ddcpl.com','demo2@mx108.in.ddcpl.com','mx108.in.ddcpl.com','2020-11-21 15:43:26','2020-11-21 15:43:26',1),('demo3@mx108.in.ddcpl.com','demo3@mx108.in.ddcpl.com','mx108.in.ddcpl.com','2020-11-21 15:43:59','2020-11-21 15:43:59',1),('demo@kcplca.com','demo@kcplca.com','kcplca.com','2020-12-22 11:35:55','2020-12-22 11:35:55',1),('demo@mx108.in.ddcpl.com','demo@mx108.in.ddcpl.com','mx108.in.ddcpl.com','2020-10-24 06:59:39','2020-10-24 06:59:39',1),('dinesh.a@kcplca.com','dinesh.a@kcplca.com','kcplca.com','2020-12-14 19:21:26','2020-12-14 19:21:26',1),('hostmaster@harshill.com','hostmaster@mx108.in.ddcpl.com','harshill.com','2020-11-12 20:04:35','2020-11-12 20:04:35',1),('hostmaster@justserverz.com','hostmaster@mx108.in.ddcpl.com','justserverz.com','2020-10-25 05:48:54','2020-10-25 05:48:54',1),('hostmaster@kcplca.com','hostmaster@mx108.in.ddcpl.com','kcplca.com','2020-12-14 19:17:57','2020-12-14 19:17:57',1),('hostmaster@mx108.in.ddcpl.com','hostmaster@mx108.in.ddcpl.com','mx108.in.ddcpl.com','2020-10-24 06:35:15','2020-10-24 06:35:15',1),('iam@harshill.com','iam@harshill.com','harshill.com','2020-11-12 20:09:43','2020-11-12 20:09:43',1),('info@justserverz.com','info@justserverz.com','justserverz.com','2020-10-26 12:32:13','2020-10-26 12:32:13',1),('krishna.m@kcplca.com','krishna.m@kcplca.com','kcplca.com','2020-12-14 19:27:52','2020-12-14 19:27:52',1),('me@harshill.com','me@harshill.com','harshill.com','2020-11-12 20:09:56','2020-11-12 20:09:56',1),('neelam.a@kcplca.com','neelam.a@kcplca.com','kcplca.com','2020-12-14 19:26:10','2020-12-14 19:26:10',1),('nikita.l@kcplca.com','nikita.l@kcplca.com','kcplca.com','2020-12-14 19:21:57','2020-12-14 19:21:57',1),('noreply@justserverz.com','noreply@justserverz.com','justserverz.com','2020-12-19 13:06:36','2020-12-19 13:06:36',1),('postmaster@harshill.com','postmaster@mx108.in.ddcpl.com','harshill.com','2020-11-12 20:04:35','2020-11-12 20:04:35',1),('postmaster@justserverz.com','postmaster@mx108.in.ddcpl.com','justserverz.com','2020-10-25 05:48:55','2020-10-25 05:48:55',1),('postmaster@kcplca.com','postmaster@mx108.in.ddcpl.com','kcplca.com','2020-12-14 19:17:57','2020-12-14 19:17:57',1),('postmaster@mx108.in.ddcpl.com','postmaster@mx108.in.ddcpl.com','mx108.in.ddcpl.com','2020-10-24 06:35:15','2020-10-24 06:35:15',1),('pranita.v@kcplca.com','pranita.v@kcplca.com','kcplca.com','2020-12-14 19:27:10','2020-12-14 19:27:10',1),('rakesh.a@kcplca.com','rakesh.a@kcplca.com','kcplca.com','2020-12-14 19:20:40','2020-12-22 14:30:44',1),('ratnakar@mx108.in.ddcpl.com','ratnakar@mx108.in.ddcpl.com','mx108.in.ddcpl.com','2020-10-25 05:53:19','2020-10-25 05:53:19',1),('shrutika.a@kcplca.com','shrutika.a@kcplca.com','kcplca.com','2020-12-14 19:22:45','2020-12-14 19:22:45',1),('sifa.k@kcplca.com','sifa.k@kcplca.com','kcplca.com','2020-12-14 19:25:29','2020-12-14 19:25:29',1),('sohail.s@kcplca.com','sohail.s@kcplca.com','kcplca.com','2020-12-14 19:24:29','2020-12-14 19:24:29',1),('sumit.y@kcplca.com','sumit.y@kcplca.com','kcplca.com','2020-12-14 19:23:34','2020-12-14 19:23:34',1),('taha.s@kcplca.com','taha.s@kcplca.com','kcplca.com','2020-12-14 19:24:54','2020-12-14 19:24:54',1),('test@kcplca.com','test@kcplca.com','kcplca.com','2020-12-22 11:31:21','2020-12-22 11:31:21',1),('vikas.r@kcplca.com','vikas.r@kcplca.com','kcplca.com','2020-12-14 19:23:08','2020-12-14 19:23:08',1),('webmaster@harshill.com','webmaster@mx108.in.ddcpl.com','harshill.com','2020-11-12 20:04:35','2020-11-12 20:04:35',1),('webmaster@justserverz.com','webmaster@mx108.in.ddcpl.com','justserverz.com','2020-10-25 05:48:56','2020-10-25 05:48:56',1),('webmaster@kcplca.com','webmaster@mx108.in.ddcpl.com','kcplca.com','2020-12-14 19:17:57','2020-12-14 19:17:57',1),('webmaster@mx108.in.ddcpl.com','webmaster@mx108.in.ddcpl.com','mx108.in.ddcpl.com','2020-10-24 06:35:15','2020-10-24 06:35:15',1);
/*!40000 ALTER TABLE `alias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alias_domain`
--

DROP TABLE IF EXISTS `alias_domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alias_domain` (
  `alias_domain` varchar(255) NOT NULL DEFAULT '',
  `target_domain` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `modified` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`alias_domain`),
  KEY `active` (`active`),
  KEY `target_domain` (`target_domain`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Postfix Admin - Domain Aliases';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alias_domain`
--

LOCK TABLES `alias_domain` WRITE;
/*!40000 ALTER TABLE `alias_domain` DISABLE KEYS */;
/*!40000 ALTER TABLE `alias_domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `value` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='PostfixAdmin settings';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,'version','1840');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domain`
--

DROP TABLE IF EXISTS `domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domain` (
  `domain` varchar(255) NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `aliases` int(10) NOT NULL DEFAULT 0,
  `mailboxes` int(10) NOT NULL DEFAULT 0,
  `maxquota` bigint(20) NOT NULL DEFAULT 0,
  `quota` bigint(20) NOT NULL DEFAULT 0,
  `transport` varchar(255) NOT NULL,
  `backupmx` tinyint(1) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `modified` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Postfix Admin - Virtual Domains';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domain`
--

LOCK TABLES `domain` WRITE;
/*!40000 ALTER TABLE `domain` DISABLE KEYS */;
INSERT INTO `domain` VALUES ('ALL','',0,0,0,0,'',0,'2020-10-24 06:34:48','2020-10-24 06:34:48',1),('harshill.com','',0,0,0,0,'virtual',0,'2020-11-12 20:04:35','2020-11-12 20:04:35',1),('justserverz.com','',0,0,0,0,'virtual',0,'2020-10-25 05:48:54','2020-12-19 13:05:29',1),('kcplca.com','',0,0,0,0,'virtual',0,'2020-12-14 19:17:57','2020-12-14 19:17:57',1),('mx108.in.ddcpl.com','INTERNAL',0,0,0,0,'virtual',0,'2020-10-24 06:35:15','2020-10-24 06:35:15',1);
/*!40000 ALTER TABLE `domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domain_admins`
--

DROP TABLE IF EXISTS `domain_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domain_admins` (
  `username` varchar(255) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Postfix Admin - Domain Admins';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domain_admins`
--

LOCK TABLES `domain_admins` WRITE;
/*!40000 ALTER TABLE `domain_admins` DISABLE KEYS */;
INSERT INTO `domain_admins` VALUES ('superadmin@mx108.in.ddcpl.com','ALL','2020-10-24 06:34:48',1);
/*!40000 ALTER TABLE `domain_admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fetchmail`
--

DROP TABLE IF EXISTS `fetchmail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fetchmail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) DEFAULT '',
  `mailbox` varchar(255) NOT NULL,
  `src_server` varchar(255) NOT NULL,
  `src_auth` enum('password','kerberos_v5','kerberos','kerberos_v4','gssapi','cram-md5','otp','ntlm','msn','ssh','any') CHARACTER SET utf8mb4 DEFAULT NULL,
  `src_user` varchar(255) NOT NULL,
  `src_password` varchar(255) NOT NULL,
  `src_folder` varchar(255) NOT NULL,
  `poll_time` int(11) unsigned NOT NULL DEFAULT 10,
  `fetchall` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `keep` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `protocol` enum('POP3','IMAP','POP2','ETRN','AUTO') CHARACTER SET utf8mb4 DEFAULT NULL,
  `usessl` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `sslcertck` tinyint(1) NOT NULL DEFAULT 0,
  `sslcertpath` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `sslfingerprint` varchar(255) DEFAULT '',
  `extra_options` text DEFAULT NULL,
  `returned_text` text DEFAULT NULL,
  `mda` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `created` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `active` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fetchmail`
--

LOCK TABLES `fetchmail` WRITE;
/*!40000 ALTER TABLE `fetchmail` DISABLE KEYS */;
/*!40000 ALTER TABLE `fetchmail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `timestamp` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `username` varchar(255) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `data` text NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `timestamp` (`timestamp`),
  KEY `domain_timestamp` (`domain`,`timestamp`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1 COMMENT='Postfix Admin - Log';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` VALUES ('2020-10-24 06:34:48','SETUP.PHP (172.16.42.47)','','create_admin','superadmin@mx108.in.ddcpl.com',1),('2020-10-24 06:35:15','superadmin@mx108.in.ddcpl.com (172.16.42.47)','mx108.in.ddcpl.com','create_domain','mx108.in.ddcpl.com',2),('2020-10-24 06:35:54','superadmin@mx108.in.ddcpl.com (172.16.42.47)','mx108.in.ddcpl.com','create_alias','demo@mx108.in.ddcpl.com',3),('2020-10-24 06:35:54','superadmin@mx108.in.ddcpl.com (172.16.42.47)','mx108.in.ddcpl.com','create_mailbox','demo@mx108.in.ddcpl.com',4),('2020-10-24 06:59:00','superadmin@mx108.in.ddcpl.com (172.16.42.47)','mx108.in.ddcpl.com','delete_mailbox','demo@mx108.in.ddcpl.com',5),('2020-10-24 06:59:39','superadmin@mx108.in.ddcpl.com (172.16.42.47)','mx108.in.ddcpl.com','create_alias','demo@mx108.in.ddcpl.com',6),('2020-10-24 06:59:40','superadmin@mx108.in.ddcpl.com (172.16.42.47)','mx108.in.ddcpl.com','create_mailbox','demo@mx108.in.ddcpl.com',7),('2020-10-24 07:08:25','superadmin@mx108.in.ddcpl.com (172.16.42.47)','mx108.in.ddcpl.com','create_alias','anto@mx108.in.ddcpl.com',8),('2020-10-24 07:08:25','superadmin@mx108.in.ddcpl.com (172.16.42.47)','mx108.in.ddcpl.com','create_mailbox','anto@mx108.in.ddcpl.com',9),('2020-10-25 05:48:56','superadmin@mx108.in.ddcpl.com (172.16.42.47)','justserverz.com','create_domain','justserverz.com',10),('2020-10-25 05:49:44','superadmin@mx108.in.ddcpl.com (172.16.42.47)','justserverz.com','create_alias','anto@justserverz.com',11),('2020-10-25 05:49:46','superadmin@mx108.in.ddcpl.com (172.16.42.47)','justserverz.com','create_mailbox','anto@justserverz.com',12),('2020-10-25 05:53:20','superadmin@mx108.in.ddcpl.com (172.16.42.47)','mx108.in.ddcpl.com','create_alias','ratnakar@mx108.in.ddcpl.com',13),('2020-10-25 05:53:21','superadmin@mx108.in.ddcpl.com (172.16.42.47)','mx108.in.ddcpl.com','create_mailbox','ratnakar@mx108.in.ddcpl.com',14),('2020-10-26 12:32:14','superadmin@mx108.in.ddcpl.com (45.118.160.213)','justserverz.com','create_alias','info@justserverz.com',15),('2020-10-26 12:32:15','superadmin@mx108.in.ddcpl.com (45.118.160.213)','justserverz.com','create_mailbox','info@justserverz.com',16),('2020-11-12 20:04:16','superadmin@mx108.in.ddcpl.com (172.16.42.61)','harshil.com','delete_domain','harshil.com',22),('2020-11-12 20:04:35','superadmin@mx108.in.ddcpl.com (172.16.42.61)','harshill.com','create_domain','harshill.com',23),('2020-11-12 20:09:43','superadmin@mx108.in.ddcpl.com (172.16.42.61)','harshill.com','create_alias','iam@harshill.com',24),('2020-11-12 20:09:43','superadmin@mx108.in.ddcpl.com (172.16.42.61)','harshill.com','create_mailbox','iam@harshill.com',25),('2020-11-12 20:09:56','superadmin@mx108.in.ddcpl.com (172.16.42.61)','harshill.com','create_alias','me@harshill.com',26),('2020-11-12 20:09:56','superadmin@mx108.in.ddcpl.com (172.16.42.61)','harshill.com','create_mailbox','me@harshill.com',27),('2020-11-12 21:25:16','superadmin@mx108.in.ddcpl.com (172.16.42.61)','harshill.com','create_alias','admin@harshill.com',28),('2020-11-12 21:25:17','superadmin@mx108.in.ddcpl.com (172.16.42.61)','harshill.com','create_mailbox','admin@harshill.com',29),('2020-11-21 15:43:26','superadmin@mx108.in.ddcpl.com (172.16.42.61)','mx108.in.ddcpl.com','create_alias','demo2@mx108.in.ddcpl.com',30),('2020-11-21 15:43:28','superadmin@mx108.in.ddcpl.com (172.16.42.61)','mx108.in.ddcpl.com','create_mailbox','demo2@mx108.in.ddcpl.com',31),('2020-11-21 15:43:59','superadmin@mx108.in.ddcpl.com (172.16.42.61)','mx108.in.ddcpl.com','create_alias','demo3@mx108.in.ddcpl.com',32),('2020-11-21 15:43:59','superadmin@mx108.in.ddcpl.com (172.16.42.61)','mx108.in.ddcpl.com','create_mailbox','demo3@mx108.in.ddcpl.com',33),('2020-12-14 19:17:57','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_domain','kcplca.com',34),('2020-12-14 19:20:40','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_alias','rakesh.a@kcplca.com',35),('2020-12-14 19:20:40','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_mailbox','rakesh.a@kcplca.com',36),('2020-12-14 19:21:26','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_alias','dinesh.a@kcplca.com',37),('2020-12-14 19:21:26','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_mailbox','dinesh.a@kcplca.com',38),('2020-12-14 19:21:57','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_alias','nikita.l@kcplca.com',39),('2020-12-14 19:21:57','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_mailbox','nikita.l@kcplca.com',40),('2020-12-14 19:22:45','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_alias','shrutika.a@kcplca.com',41),('2020-12-14 19:22:45','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_mailbox','shrutika.a@kcplca.com',42),('2020-12-14 19:23:08','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_alias','vikas.r@kcplca.com',43),('2020-12-14 19:23:08','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_mailbox','vikas.r@kcplca.com',44),('2020-12-14 19:23:34','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_alias','sumit.y@kcplca.com',45),('2020-12-14 19:23:34','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_mailbox','sumit.y@kcplca.com',46),('2020-12-14 19:24:29','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_alias','sohail.s@kcplca.com',47),('2020-12-14 19:24:29','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_mailbox','sohail.s@kcplca.com',48),('2020-12-14 19:24:54','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_alias','taha.s@kcplca.com',49),('2020-12-14 19:24:54','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_mailbox','taha.s@kcplca.com',50),('2020-12-14 19:25:29','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_alias','sifa.k@kcplca.com',51),('2020-12-14 19:25:29','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_mailbox','sifa.k@kcplca.com',52),('2020-12-14 19:26:10','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_alias','neelam.a@kcplca.com',53),('2020-12-14 19:26:10','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_mailbox','neelam.a@kcplca.com',54),('2020-12-14 19:27:10','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_alias','pranita.v@kcplca.com',55),('2020-12-14 19:27:10','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_mailbox','pranita.v@kcplca.com',56),('2020-12-14 19:27:52','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_alias','krishna.m@kcplca.com',57),('2020-12-14 19:27:52','Superadmin@mx108.in.ddcpl.com (45.119.46.84)','kcplca.com','create_mailbox','krishna.m@kcplca.com',58),('2020-12-19 13:05:30','superadmin@mx108.in.ddcpl.com (45.118.160.215)','justserverz.com','edit_domain','justserverz.com',59),('2020-12-19 13:06:36','superadmin@mx108.in.ddcpl.com (45.118.160.215)','justserverz.com','create_alias','noreply@justserverz.com',60),('2020-12-19 13:06:36','superadmin@mx108.in.ddcpl.com (45.118.160.215)','justserverz.com','create_mailbox','noreply@justserverz.com',61),('2020-12-22 11:31:21','superadmin@mx108.in.ddcpl.com (172.16.42.61)','kcplca.com','create_alias','test@kcplca.com',62),('2020-12-22 11:31:21','superadmin@mx108.in.ddcpl.com (172.16.42.61)','kcplca.com','create_mailbox','test@kcplca.com',63),('2020-12-22 11:35:55','superadmin@mx108.in.ddcpl.com (172.16.42.61)','kcplca.com','create_alias','demo@kcplca.com',64),('2020-12-22 11:35:55','superadmin@mx108.in.ddcpl.com (172.16.42.61)','kcplca.com','create_mailbox','demo@kcplca.com',65),('2020-12-22 14:30:44','superadmin@mx108.in.ddcpl.com (172.16.42.61)','kcplca.com','edit_alias','rakesh.a@kcplca.com',66),('2020-12-22 14:30:44','superadmin@mx108.in.ddcpl.com (172.16.42.61)','kcplca.com','edit_mailbox','rakesh.a@kcplca.com',67);
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailbox`
--

DROP TABLE IF EXISTS `mailbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailbox` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `maildir` varchar(255) NOT NULL,
  `quota` bigint(20) NOT NULL DEFAULT 0,
  `local_part` varchar(255) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `modified` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `phone` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `email_other` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `token` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `token_validity` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  PRIMARY KEY (`username`),
  KEY `domain` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Postfix Admin - Virtual Mailboxes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailbox`
--

LOCK TABLES `mailbox` WRITE;
/*!40000 ALTER TABLE `mailbox` DISABLE KEYS */;
INSERT INTO `mailbox` VALUES ('admin@harshill.com','$1$9373df45$9u/DsJdrFmkuFgwL8iViy0','','harshill.com/admin/',0,'admin','harshill.com','2020-11-12 21:25:16','2020-11-12 21:25:16',1,'','','','2020-11-12 21:25:16'),('anto@justserverz.com','$1$0f11f88e$8JQLd2RB5LyICTE84hUEd.','Just ServerZ','justserverz.com/anto/',0,'anto','justserverz.com','2020-10-25 05:49:44','2020-10-25 05:49:44',1,'','','','2020-10-25 05:49:42'),('anto@mx108.in.ddcpl.com','$1$IQueapD5$sNwRBd6kgk7l7FCG13ogi/','','mx108.in.ddcpl.com/anto/',0,'anto','mx108.in.ddcpl.com','2020-10-24 07:08:25','2020-10-25 06:05:40',1,'','','','2020-10-24 07:08:25'),('demo2@mx108.in.ddcpl.com','$1$7459d144$xaIfIYQlfboTHLUK2WlXe/','demo2','mx108.in.ddcpl.com/demo2/',0,'demo2','mx108.in.ddcpl.com','2020-11-21 15:43:27','2020-11-21 15:43:27',1,'','','','2020-11-21 15:43:26'),('demo3@mx108.in.ddcpl.com','$1$cc84ebe8$Rwj0GEk.AXsIjE8VzcPyF.','demo3','mx108.in.ddcpl.com/demo3/',0,'demo3','mx108.in.ddcpl.com','2020-11-21 15:43:59','2020-11-21 15:43:59',1,'','','','2020-11-21 15:43:58'),('demo@kcplca.com','$1$67cf6ae9$lRMFhkYuGaFwqJcDJPdke1','','kcplca.com/demo/',0,'demo','kcplca.com','2020-12-22 11:35:55','2020-12-22 11:35:55',1,'','','','2020-12-22 11:35:55'),('demo@mx108.in.ddcpl.com','$1$649c2da2$UWvuYqoeED8GWZbMjFa1F.','DEMO USER','mx108.in.ddcpl.com/demo/',0,'demo','mx108.in.ddcpl.com','2020-10-24 06:59:40','2020-10-24 06:59:40',1,'','','','2020-10-24 06:59:39'),('dinesh.a@kcplca.com','$1$fe643585$WaDDM9N4MxXmpn1Rka5410','Dinesh Agrawal','kcplca.com/dinesh.a/',0,'Dinesh.a','kcplca.com','2020-12-14 19:21:26','2020-12-14 19:21:26',1,'','','','2020-12-14 19:21:26'),('iam@harshill.com','$1$e58619db$tHJRg39Vv3Swp9XDSjgQl0','','harshill.com/iam/',0,'iam','harshill.com','2020-11-12 20:09:43','2020-11-12 20:09:43',1,'','','','2020-11-12 20:09:43'),('info@justserverz.com','$1$344f04ca$yuAHkcWJPWHuggJ.Jgmdh/','info','justserverz.com/info/',0,'info','justserverz.com','2020-10-26 12:32:14','2020-10-26 12:32:14',1,'','','','2020-10-26 12:32:11'),('krishna.m@kcplca.com','$1$32e096c0$gAeV0Fakv.6f9Wrs8m14L/','Krishna Mali','kcplca.com/krishna.m/',0,'Krishna.m','kcplca.com','2020-12-14 19:27:52','2020-12-14 19:27:52',1,'','','','2020-12-14 19:27:52'),('me@harshill.com','$1$9bf91a63$VANkZETpwm8MAQv9TlLeB1','','harshill.com/me/',0,'me','harshill.com','2020-11-12 20:09:56','2020-11-12 20:09:56',1,'','','','2020-11-12 20:09:56'),('neelam.a@kcplca.com','$1$26fa54e9$.inijlvAVZXcIi4R8dow71','Neelam Ashar','kcplca.com/neelam.a/',0,'Neelam.a','kcplca.com','2020-12-14 19:26:10','2020-12-14 19:26:10',1,'','','','2020-12-14 19:26:10'),('nikita.l@kcplca.com','$1$13eb94b0$bZe.55FnhPZtf5APFhju40','Nikita Lad','kcplca.com/nikita.l/',0,'Nikita.l','kcplca.com','2020-12-14 19:21:57','2020-12-14 19:21:57',1,'','','','2020-12-14 19:21:57'),('noreply@justserverz.com','$1$321a868c$dgfC46W9O29jjubBYDbAr1','No Reply','justserverz.com/noreply/',0,'noreply','justserverz.com','2020-12-19 13:06:36','2020-12-19 13:06:36',1,'','','','2020-12-19 13:06:34'),('pranita.v@kcplca.com','$1$5e7013ce$mPkCqCHQ1jRby/HzF6Wgo.','Pranita Vanarase','kcplca.com/pranita.v/',0,'Pranita.v','kcplca.com','2020-12-14 19:27:10','2020-12-14 19:27:10',1,'','','','2020-12-14 19:27:10'),('rakesh.a@kcplca.com','$1$c5bf1b82$9gL9oT5rRBA5FLVc05I0m1','Rakesh Agarwal','kcplca.com/rakesh.a/',0,'Rakesh.a','kcplca.com','2020-12-14 19:20:40','2020-12-22 14:30:44',1,'','','','2020-12-14 19:20:39'),('ratnakar@mx108.in.ddcpl.com','$1$47378636$oVLuqkuu/a9W5BzfGF6FS.','Ratnakar','mx108.in.ddcpl.com/ratnakar/',0,'ratnakar','mx108.in.ddcpl.com','2020-10-25 05:53:20','2020-10-25 05:53:20',1,'','','','2020-10-25 05:53:19'),('shrutika.a@kcplca.com','$1$e86dacd4$nZPjUmJtDhRp0zsiWOccU/','Shrutika Agrawal','kcplca.com/shrutika.a/',0,'Shrutika.a','kcplca.com','2020-12-14 19:22:45','2020-12-14 19:22:45',1,'','','','2020-12-14 19:22:45'),('sifa.k@kcplca.com','$1$pu0Pg2Ho$K4gckGBMlBbOSLoTFIWzo.','Sifa Khan','kcplca.com/sifa.k/',0,'Sifa.k','kcplca.com','2020-12-14 19:25:29','2020-12-22 16:43:13',1,'','','','2020-12-14 19:25:29'),('sohail.s@kcplca.com','$1$0fb5c640$elADNZ.8wcVBmtHdC445I0','Sohail Sayyed','kcplca.com/sohail.s/',0,'Sohail.s','kcplca.com','2020-12-14 19:24:29','2020-12-14 19:24:29',1,'','','','2020-12-14 19:24:29'),('sumit.y@kcplca.com','$1$4e799ca6$8rx3//A/8GT76GEv6n/M81','Sumit Yadav','kcplca.com/sumit.y/',0,'Sumit.y','kcplca.com','2020-12-14 19:23:34','2020-12-14 19:23:34',1,'','','','2020-12-14 19:23:34'),('taha.s@kcplca.com','$1$f0311367$dg0LigcMU/th/7Cikcyae1','Taha Shaikh','kcplca.com/taha.s/',0,'Taha.s','kcplca.com','2020-12-14 19:24:54','2020-12-14 19:24:54',1,'','','','2020-12-14 19:24:54'),('test@kcplca.com','$1$6daf45c3$Hwx5REA92mlfyLpbh5wo00','','kcplca.com/test/',0,'test','kcplca.com','2020-12-22 11:31:21','2020-12-22 11:31:21',1,'','','','2020-12-22 11:31:20'),('vikas.r@kcplca.com','$1$6c09fdf2$WxR.TMYiDKeKy6BfgRF/30','Vikas Rajput','kcplca.com/vikas.r/',0,'Vikas.r','kcplca.com','2020-12-14 19:23:08','2020-12-14 19:23:08',1,'','','','2020-12-14 19:23:07');
/*!40000 ALTER TABLE `mailbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quota`
--

DROP TABLE IF EXISTS `quota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quota` (
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `path` varchar(100) CHARACTER SET latin1 NOT NULL,
  `current` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`username`,`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quota`
--

LOCK TABLES `quota` WRITE;
/*!40000 ALTER TABLE `quota` DISABLE KEYS */;
/*!40000 ALTER TABLE `quota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quota2`
--

DROP TABLE IF EXISTS `quota2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quota2` (
  `username` varchar(100) CHARACTER SET latin1 NOT NULL,
  `bytes` bigint(20) NOT NULL DEFAULT 0,
  `messages` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quota2`
--

LOCK TABLES `quota2` WRITE;
/*!40000 ALTER TABLE `quota2` DISABLE KEYS */;
/*!40000 ALTER TABLE `quota2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacation`
--

DROP TABLE IF EXISTS `vacation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacation` (
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `activefrom` timestamp NOT NULL DEFAULT '1999-12-31 18:30:00',
  `activeuntil` timestamp NOT NULL DEFAULT '2038-01-17 18:30:00',
  `cache` text NOT NULL,
  `domain` varchar(255) NOT NULL,
  `interval_time` int(11) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`email`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Postfix Admin - Virtual Vacation';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacation`
--

LOCK TABLES `vacation` WRITE;
/*!40000 ALTER TABLE `vacation` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacation_notification`
--

DROP TABLE IF EXISTS `vacation_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacation_notification` (
  `on_vacation` varchar(255) CHARACTER SET latin1 NOT NULL,
  `notified` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `notified_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`on_vacation`,`notified`),
  CONSTRAINT `vacation_notification_pkey` FOREIGN KEY (`on_vacation`) REFERENCES `vacation` (`email`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Postfix Admin - Virtual Vacation Notifications';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacation_notification`
--

LOCK TABLES `vacation_notification` WRITE;
/*!40000 ALTER TABLE `vacation_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacation_notification` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-27 13:50:21
